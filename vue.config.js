module.exports = {
  baseUrl: process.env.VUE_BASE_URL || '/',
  devServer: {
    public: `${process.env.HOST}:${process.env.PORT}`,
  },
  lintOnSave: 'error',
};
