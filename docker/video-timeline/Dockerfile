FROM node:10.6.0
LABEL maintainer "incoming+johnsardine/video-timeline@incoming.gitlab.com"

ENV PROJECT_DIR /srv/video-timeline
WORKDIR $PROJECT_DIR

ARG VUE_APP_FULLSTORY_ENABLED
ARG VUE_APP_SENTRY_ENABLED
ARG VUE_APP_SENTRY_DSN_PUBLIC
ARG VUE_APP_SENTRY_RELEASE
ARG VUE_APP_SENTRY_VERSION_TAG
ARG VUE_APP_API_ROOT
ARG VUE_APP_PUSHER_APP_KEY
ARG VUE_APP_PUSHER_APP_CLUSTER
ARG VUE_APP_ENVIRONMENT
ARG VUE_BASE_URL

# Client Related
COPY ./src src
COPY ./public public
COPY ./tests tests
COPY ./.eslintrc.js .
COPY ./.postcssrc.js .
COPY ./babel.config.js .
COPY ./cypress.json .
COPY ./jest.config.js .

# Common
COPY ./package-lock.json .
COPY ./package.json .

ENV CYPRESS_INSTALL_BINARY=0

RUN npm ci

RUN npm run build

FROM nginx:1.15.2
WORKDIR /usr/share/nginx/html/
COPY --from=0 /srv/video-timeline/dist/ .
COPY ./docker/video-timeline/nginx.conf /etc/nginx/nginx.conf
